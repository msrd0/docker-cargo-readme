FROM alpine:3.11

ENV CARGO_HOME=/usr/local/cargo

RUN apk add --no-cache cargo rust \
	&& cargo install cargo-readme --git https://github.com/livioribeiro/cargo-readme \
	&& strip "$CARGO_HOME/bin/cargo-readme" \
	&& mv "$CARGO_HOME/bin/cargo-readme" /usr/bin \
	&& rm -rf "$CARGO_HOME"
